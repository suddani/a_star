/*
  Copyright (C) 2010 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/
#include "a_star/a_pathfinding.h"
#include <stdlib.h>

SNodeList* nodelist_create(void)
{
    SNodeList* list = malloc(sizeof(SNodeList));
    list->first = 0;
    list->last  = 0;
    return list;
}

void nodelist_delete(SNodeList* list)
{
    while (list->first != 0)
        nodelist_erase(list, list->first);
}

int nodelist_empty(SNodeList* list)
{
    if (list->first == 0 && list->last == 0)
        return 0;
    return 1;
}

SNodeListElement* nodelist_push_back(SNodeList* list, SNode* node)
{
    SNodeListElement* e = nodelistelement_create();
    e->node = node;
    if (nodelist_empty(list) == 0)
        list->first = e;

    if (list->last != 0)
        list->last->next = e;

    e->previous = list->last;
    list->last = e;
    return e;
}

SNodeListElement* nodelist_push_front(SNodeList* list, SNode* node)
{
    SNodeListElement* e = nodelistelement_create();
    e->node = node;
    if (nodelist_empty(list) == 0)
        list->last = e;

    if (list->first)
        list->first->previous = e;

    e->next = list->first;
    list->first = e;
    return e;
}

SNodeListElement* nodelist_insert_after(SNodeList* list, SNode* node, SNodeListElement* after)
{
    SNodeListElement* e = nodelistelement_create();
    e->node = node;

    e->next = after->next;
    e->previous = after;

    if (after->next)
        after->next->previous = e;
    after->next = e;

    if (list->last == after)
        list->last = e;

    return e;
}

SNodeListElement* nodelist_insert_before(SNodeList* list, SNode* node, SNodeListElement* before)
{
    SNodeListElement* e = nodelistelement_create();
    e->node = node;

    e->next = before;
    e->previous = before->previous;

    if (before->previous)
        before->previous->next = e;
    before->previous = e;

    if (list->first == before)
        list->first = e;

    return e;
}

SNodeListElement* nodelist_erase(SNodeList* list, SNodeListElement* e)
{
    if (e->previous)
        e->previous->next = e->next;
    if (e->next)
        e->next->previous = e->previous;

    if (list->first == e)
        list->first = e->next;
    if (list->last == e)
        list->last = e->previous;

    SNodeListElement* next = e->next;

    nodelistelement_delete(e);

    return next;
}
