/*
  Copyright (C) 2010 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/
namespace ai
{
    extern "C"
    {
    #include "a_star/a_pathfinding.h"
    }
}
#include <stdio.h>
#include <time.h>



bool check(ai::SNode* node, int x, int y)
{
    if (node->x == x && node->y == y)
        return true;
    else if (node->parent)
        return check(node->parent, x,y);
    return false;
}

int pathcount(ai::SNode* node)
{
    if (node->parent)
        return 1+pathcount(node->parent);
    return 0;
}

int searchedNodes(ai::SNodeMesh* mesh)
{
    int count = 0;
    for (int i=0;i<mesh->width*mesh->height;++i)
        if (mesh->mesh[i]->parent)
            count++;
    return count;
}

int main(int argc, char* args[])
{
    ai::SNodeMesh* nodeMesh = ai::nodemesh_create(70,70);

    for (int i=1;i<69;++i)
        ai::nodemesh_get_node(nodeMesh, 20, i)->walkable = 1;

    for (int i=1;i<51;++i)
        ai::nodemesh_get_node(nodeMesh, i, 30)->walkable = 1;

    ai::SNode* start = ai::nodemesh_get_node(nodeMesh, 18, 10);
    ai::SNode* end = ai::nodemesh_get_node(nodeMesh, 50, 50);
    int s = clock();
    ai::SNode* node = ai::a_findpath(start, end);
    int e = clock();

    for (int x=0; x<nodeMesh->width; ++x)
    {
        for (int y=0; y<nodeMesh->height; ++y)
        {
            if (x == start->x && y == start->y)
                printf("S");
            else if (node && x == node->x && y == node->y)
                printf("E");
            else if (node && check(node, x, y))
                printf("1");
            else if (ai::nodemesh_get_node(nodeMesh, x,y)->walkable == 1)
                printf("b");
            else if (ai::nodemesh_get_node(nodeMesh, x,y)->parent)
                printf("c");
            else
                printf("0");
        }
        printf("\n");
    }

    printf("Time: %i\n", e-s);

    if (node)
       printf("Found path: %i and searched %i nodes\n", pathcount(node), searchedNodes(nodeMesh));
    else
        printf("Could not find path\n");

    printf("End Programm\n");
    return 0;
}
