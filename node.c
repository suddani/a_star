/*
  Copyright (C) 2010 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/
#include "a_star/a_pathfinding.h"
#include <stdlib.h>

SNode* node_create(int x, int y)
{
    SNode* node = malloc(sizeof(SNode));
    node->parent = 0;

    node->openlist = list_type_none;
    node->list = 0;
    node->element = 0;

    int n=0;
    for (;n<8;++n)
        node->neighbors[n]  = 0;

    node->x = x;
    node->y = y;
    node->G = 0;
    node->H = 0;
    node->walkable = 0;
    return node;
}

void node_delete(SNode* node)
{
    node->parent = 0;
    free(node);
}

void node_setup(SNode* node, SNodeMesh* mesh)
{
    node->neighbors[0] = nodemesh_get_node(mesh, node->x, node->y+1);
    node->neighbors[1] = nodemesh_get_node(mesh, node->x, node->y-1);
    node->neighbors[2] = nodemesh_get_node(mesh, node->x-1, node->y);
    node->neighbors[3] = nodemesh_get_node(mesh, node->x+1, node->y);
    node->neighbors[4] = nodemesh_get_node(mesh, node->x-1, node->y+1);
    node->neighbors[5] = nodemesh_get_node(mesh, node->x+1, node->y+1);
    node->neighbors[6] = nodemesh_get_node(mesh, node->x-1, node->y-1);
    node->neighbors[7] = nodemesh_get_node(mesh, node->x+1, node->y-1);
    /*
    node->up          = nodemesh_get_node(mesh, node->x, node->y+1);
    node->down        = nodemesh_get_node(mesh, node->x, node->y-1);
    node->left        = nodemesh_get_node(mesh, node->x-1, node->y);
    node->right       = nodemesh_get_node(mesh, node->x+1, node->y);
    node->upleft      = nodemesh_get_node(mesh, node->x-1, node->y+1);
    node->upright     = nodemesh_get_node(mesh, node->x+1, node->y+1);
    node->downleft    = nodemesh_get_node(mesh, node->x-1, node->y-1);
    node->downright   = nodemesh_get_node(mesh, node->x+1, node->y-1);
    */
}

int node_get_f_score(SNode* node)
{
    return node->G+node->H*10;
}

int node_get_h_score(SNode* node, SNode* target)
{
    int cost = abs(node->x - target->x)+abs(node->y - target->y);
    return cost;

    if (node->x > target->x)
        cost += node->x - target->x;
    else
        cost += target->x - node->x;
    if (node->y > target->y)
        cost += node->y - target->y;
    else
        cost += target->y - node->y;

    return cost;
}
