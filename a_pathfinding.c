/*
  Copyright (C) 2010 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/
#include "a_star/a_pathfinding.h"
#include <stdlib.h>

const int list_type_none = 0;
const int list_type_open = 1;
const int list_type_closed = 2;

void f_sort_release_list(SNodeList* list)
{
    SNodeListElement* e = list->first;
    while (e != 0)
    {
        e->node->list = 0;
        e->node->element = 0;
        e->node->openlist = list_type_none;
        e = nodelist_erase(list, e);
    }
    nodelist_delete(list);
}

void f_sort_remove(SNode* node)
{
    if (node->openlist != 0)
    {
        nodelist_erase(node->list, node->element);
        node->list = 0;
        node->element = 0;
        node->openlist = list_type_none;
    }
}

SNodeListElement* f_sort_insert(SNodeList* list, SNode* node, int listtype)
{
    f_sort_remove(node);


    node->openlist = listtype;
    node->list = list;

    if (nodelist_empty(list) == 0 || listtype == list_type_closed)
    {
        node->element = nodelist_push_back(list, node);
        return node->element;
    }
    else
    {
        SNodeListElement* e = list->first;
        while (e != 0)
        {
            if (node_get_f_score(node) < node_get_f_score(e->node))
            {
                node->element = nodelist_insert_before(list, node, e);

                return node->element;
            }
            e = e->next;
        }
        node->element = nodelist_push_back(list, node);

        return node->element;
    }
}

SNode* a_pathfinding(SNodeList* openlist, SNodeList* closedlist, SNode* target)
{
    if (nodelist_empty(openlist) == 0)
        return 0;

    SNode* current = openlist->first->node;

    if (current == target)
        return current;

    f_sort_insert(closedlist, current, list_type_closed);

    int n=0;
    for(;n<8;++n)
    {
        if (current->neighbors[n] != 0)
        {
            int Gcost = current->G + (n<4?10:14);
            if (current->neighbors[n]->openlist != list_type_closed && current->neighbors[n]->walkable == 0)
            {
                if (current->neighbors[n]->openlist != list_type_open)
                {
                    current->neighbors[n]->H = node_get_h_score(current->neighbors[n], target);
                    current->neighbors[n]->G = Gcost;
                    current->neighbors[n]->parent = current;
                    f_sort_insert(openlist, current->neighbors[n], list_type_open);
                }
                else// if (current->neighbors[n]->openlist == list_type_open)
                {
                    if (Gcost < current->neighbors[n]->G)
                    {
                        current->neighbors[n]->H = node_get_h_score(current->neighbors[n], target);
                        current->neighbors[n]->G = Gcost;
                        current->neighbors[n]->parent = current;

                        f_sort_insert(openlist, current->neighbors[n], list_type_open);
                    }
                }
            }
        }
    }

    return a_pathfinding(openlist, closedlist, target);
}

SNode* a_findpath(SNode* start, SNode* target)
{
    SNodeList* openlist = nodelist_create();
    SNodeList* closedlist = nodelist_create();

    start->parent = 0;
    start->H = 0;
    start->G = 0;
    f_sort_insert(openlist, start, list_type_open);

    SNode* t = a_pathfinding(openlist, closedlist, target);

    f_sort_release_list(openlist);
    f_sort_release_list(closedlist);

    return t;
}



