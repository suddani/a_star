#ifndef A_PATHFINDING_H
#define A_PATHFINDING_H
extern const int list_type_none;
extern const int list_type_open;
extern const int list_type_closed;

typedef struct Node_
{
    int x;
    int y;

    int G;
    int H;

    int walkable; //0=true, 1=false

    struct NodeList_* list;
    struct NodeListElement_* element;
    int openlist; // 0=none, 1=openlist, 2=closedlist

    struct Node_* neighbors[8];

    struct Node_* parent;
}SNode;

typedef struct NodeMesh_
{
    int width;
    int height;
    SNode** mesh;
}SNodeMesh;

typedef struct NodeListElement_
{
    SNode* node;
    struct NodeListElement_* previous;
    struct NodeListElement_* next;
}SNodeListElement;

typedef struct NodeList_
{
    struct NodeListElement_* first;
    struct NodeListElement_* last;
}SNodeList;

//!Node
SNode* node_create(int x, int y);
void node_delete(SNode* node);
void node_setup(SNode* node, SNodeMesh* mesh);
int node_get_f_score(SNode* node);
int node_get_h_score(SNode* node, SNode* target);

//!NodeMesh
SNodeMesh* nodemesh_create(int width, int height);
void nodemesh_delete(SNodeMesh* m);
void nodemesh_unparent_all(SNodeMesh* m);
SNode* nodemesh_get_node(SNodeMesh* m, int x, int y);

//!NodeListElement
SNodeListElement* nodelistelement_create(void);
void nodelistelement_delete(SNodeListElement* e);

//!NodeList
SNodeList* nodelist_create(void);
void nodelist_delete(SNodeList* list);
int nodelist_empty(SNodeList* list);
SNodeListElement* nodelist_push_back(SNodeList* list, SNode* node);
SNodeListElement* nodelist_push_front(SNodeList* list, SNode* node);
SNodeListElement* nodelist_insert_after(SNodeList* list, SNode* node, SNodeListElement* after);
SNodeListElement* nodelist_insert_before(SNodeList* list, SNode* node, SNodeListElement* before);
SNodeListElement* nodelist_erase(SNodeList* list, SNodeListElement* e);

//!A*
/** \brief
 *
 * \param start SNode* the start node
 * \param target SNode* the target node
 * \return SNode* returns the target node if a path is found
 *
 */
SNode* a_findpath(SNode* start, SNode* target);
#endif
