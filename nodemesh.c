/*
  Copyright (C) 2010 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/
#include "a_star/a_pathfinding.h"
#include <stdlib.h>

SNodeMesh* nodemesh_create(int width, int height)
{
    SNodeMesh* m = malloc(sizeof(SNodeMesh));
    m->width = width;
    m->height = height;

    m->mesh = malloc(m->width*m->height*sizeof(SNode*));

    int x = 0;
    int y = 0;
    for (y=0;y<m->height;++y)
    {
        for (x=0;x<m->width;++x)
        {
            m->mesh[x+y*m->width] = node_create(x,y);
        }
    }
    int i=0;
    int size=m->width*m->height;
    for (i=0;i<size;++i)
        node_setup(m->mesh[i], m);

    return m;
}

void nodemesh_delete(SNodeMesh* m)
{
    int i=0;
    int size=m->height*m->width;
    for (i=0;i<size;++i)
        node_delete(m->mesh[i]);
    free(m->mesh);
    free(m);
}

void nodemesh_unparent_all(SNodeMesh* m)
{
    int i=0;
    int size=m->width*m->height;
    for (i=0;i<size;++i)
        m->mesh[i]->parent = 0;
}

SNode* nodemesh_get_node(SNodeMesh* m, int x, int y)
{
    if (x < 0 || y < 0 || x >= m->width || y >= m->height)
        return 0;
    int apos = x+y*m->width;

    if (apos > m->height*m->width)
        return 0;

    return m->mesh[apos];
}
